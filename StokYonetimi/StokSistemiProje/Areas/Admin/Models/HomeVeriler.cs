﻿using Microsoft.AspNet.Identity.EntityFramework;
using StokSistemiProje.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StokSistemiProje.Areas.Admin.Models
{
    public class HomeVeriler
    {
        
        public int personel_sayi { get; set; }
        public int musteri_sayi { get; set; }
        public int tedarikci_sayi { get; set; }
        public int toplam_urun { get; set; }


        public List<Kisi> Kisi { get; set; }
        public List<Musteri> Musteri { get; set; }
        public List<Calisan> Calisan { get; set; }
        public List<Urun> Urun { get; set; }
        public List<Urun_Satislar> urun_satis { get; set; }
        public List<Satislar> satis { get; set; }
        public List<Tedarikci> tedarikci { get; set; }
    }
}