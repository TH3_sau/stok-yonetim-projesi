﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(StokSistemiProje.Startup))]
namespace StokSistemiProje
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
