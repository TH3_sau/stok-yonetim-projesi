﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using StokSistemiProje.Models;
using StokSistemiProje.Areas.Admin.Models;

namespace StokSistemiProje.Areas.Admin.Controllers
{
    public class MainController : Controller
    {
        Entities db = new Entities();
        // GET: Admin/Main
        public ActionResult Index()
        {
            HomeVeriler model = new HomeVeriler();
            model.musteri_sayi = db.Musteri.Count();
            model.personel_sayi = db.Calisan.Count();

            model.toplam_urun = db.Urun.Count();

            return View(model);
        }
    }
}