﻿using StokYonetimi;
using System.Web;
using System.Web.Mvc;

namespace StokSistemiProje
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        { 
            filters.Add(new HandleErrorAttribute());
            filters.Add(new LanguageAttribute());
        }
    }
}
