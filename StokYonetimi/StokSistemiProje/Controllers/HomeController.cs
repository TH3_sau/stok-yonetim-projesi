﻿using Microsoft.AspNet.Identity;
using StokYonetimi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using StokSistemiProje.Models;

namespace StokSistemiProje.Controllers
{
    [Language]

    public class HomeController : Controller
    {
        Entities db = new Entities();

        public ActionResult Index()
        {

            return View();
        }

      [HttpPost]
        public ActionResult Index(Mesajlar mesaj)
        {

            Mesajlar musteri = new Mesajlar();
            musteri.Konu = mesaj.Konu;
            musteri.Mesaj = mesaj.Mesaj;
         
            db.Mesajlar.Add(musteri);
            db.SaveChanges();

            return RedirectToAction("Index","Home");
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}